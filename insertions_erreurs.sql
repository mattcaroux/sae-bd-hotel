-- Cette insertion ne marche pas car le type de la colonne est un entier et non une chaine de caractère
INSERT INTO HOTEL VALUES ('Hilton', 'Paris', 'France', '5', '5');

-- Cette insertion ne marche pas car la numero de l'hotel n'existe pas, 696 n'est pas un numero d'hotel
INSERT INTO CHAMBRE VALUES ('100', 3, 35, 696);

-- Cette insertions ne marche pas car le numero d'hotel n'existe pas ainsi que le numero identifiant du client
INSERT INTO RESERVER VALUES (TO_DATE('2023-01-02', 'YYYY-MM-DD'), TO_DATE('2023-01-05', 'YYYY-MM-DD'), 754134, 696, '101');

-- Cette fonction ne marche pas a cause d'un probleme avec le nom du département car le département n'est pas créé
INSERT INTO COMMANDE VALUES ('Commande3', TO_DATE('2023-03-02', 'YYYY-MM-DD'), 365, 4, 'Y', 'Service blanchisserie', '3', 1);

-- Cette suite ne marche pas car il faut d'abord mettre le département et ensuite mettre la commande
INSERT INTO COMMANDE VALUES ('Commande3', TO_DATE('2023-03-02', 'YYYY-MM-DD'), 365, 4, 'Y', 'Service blanchisserie', '3', 1);
INSERT INTO DEPARTEMENT VALUES ('Service blanchisserie', 'Directeur Mengo', 0607080910, 2);
