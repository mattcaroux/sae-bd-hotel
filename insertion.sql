-- Ajout d'entrees dans la table HOTEL
INSERT INTO HOTEL (code_h, nom_h, address, nb_chambre, qualite) VALUES (1, 'Hotel A', 'Adresse A', 50, 4);
INSERT INTO HOTEL (code_h, nom_h, address, nb_chambre, qualite) VALUES (2, 'Hotel B', 'Adresse B', 100, 5);



-- Ajout d'entrees dans la table PERSONNE
INSERT INTO PERSONNE (num_ident, nom, adresse, date_naissance) VALUES (1, 'John Doe', '123 Main Street', TO_DATE('1990-01-01', 'YYYY-MM-DD'));
INSERT INTO PERSONNE (num_ident, nom, adresse, date_naissance) VALUES (2, 'Matthias Caroux', '83 rue vielle levee', TO_DATE('2003-07-29', 'YYYY-MM-DD'));
insert into PERSONNE (num_ident, nom, adresse, date_naissance) values (3, 'Dubois, Pierette', '789 rue de la Joie', TO_DATE('1975-12-10' , 'YYYY-MM-DD'));
insert into PERSONNE (num_ident, nom, adresse, date_naissance) values (4, 'Lefevre, Isabelle', '1010 boulevard du Soleil', TO_DATE('1988-07-03' , 'YYYY-MM-DD'));
insert into PERSONNE (num_ident, nom, adresse, date_naissance) values (5, 'Leroy, Michel', '2020 avenue de la Plage', TO_DATE('1965-03-22'  , 'YYYY-MM-DD' ));



-- Ajout d'entrees dans la table CLIENT
INSERT INTO CLIENT (nationalite, num_ident) VALUES ('Francaise', 1);
INSERT INTO CLIENT (nationalite, num_ident) VALUES ('Canadienne', 2);
INSERT INTO CLIENT (nationalite, num_ident) VALUES ('Espagnole', 3);




-- Ajout d'entrees dans la table PERSONNEL 
INSERT INTO PERSONNEL (fonction, num_ident) VALUES ('Femme de Menage', 3);
INSERT INTO PERSONNEL (fonction, num_ident) VALUES ('Receptionniste', 4);
INSERT INTO PERSONNEL (fonction, num_ident) VALUES ('Cuisinier', 5);



-- Ajout d'entrees dans la table CHAMBRE
-- Insertion pour le premier hotel qui accueil 50 chambres
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('1', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('2', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('3', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('4', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('5', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('6', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('7', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('8', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('9', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('10', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('11', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('12', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('13', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('14', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('15', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('16', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('17', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('18', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('19', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('20', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('21', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('22', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('23', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('24', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('25', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('26', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('27', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('28', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('29', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('30', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('31', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('32', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('33', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('34', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('35', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('36', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('37', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('38', 3, 35, 1);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('39', 3, 35, 1);
-- Insertion pour le deuxieme hotel qui accueil 100 chambres
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('1', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('2', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('3', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('4', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('5', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('6', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('7', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('8', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('9', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('10', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('11', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('12', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('13', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('14', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('15', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('16', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('17', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('18', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('19', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('20', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('21', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('22', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('23', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('24', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('25', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('26', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('27', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('28', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('29', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('30', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('31', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('32', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('33', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('34', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('35', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('36', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('37', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('38', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('39', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('40', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('41', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('42', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('43', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('44', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('45', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('46', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('47', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('48', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('49', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('50', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('51', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('52', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('53', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('54', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('55', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('56', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('57', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('58', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('59', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('60', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('61', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('62', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('63', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('64', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('65', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('66', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('67', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('68', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('69', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('70', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('71', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('72', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('73', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('74', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('75', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('76', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('77', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('78', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('79', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('80', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('81', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('82', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('83', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('84', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('85', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('86', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('87', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('88', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('89', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('90', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('91', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('92', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('93', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('94', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('95', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('96', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('97', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('98', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('99', 3, 35, 2);
INSERT INTO CHAMBRE (num_chambre, capacite_max, superficie, code_h) VALUES ('100', 3, 35, 2);






-- Ajout d'entrees dans la table DEPARTEMENT

-- Pour le premier hotel
INSERT INTO DEPARTEMENT (nom_departement, directeur, num_tel, code_h) VALUES ('Service Clientele', 'Directeur Patrick', 123456789, 1);
INSERT INTO DEPARTEMENT (nom_departement, directeur, num_tel, code_h) VALUES ('Cuisine', 'Directeur Cuisto', 0607080910, 1);
INSERT INTO DEPARTEMENT (nom_departement, directeur, num_tel, code_h) VALUES ('Reception', 'Directeur Reception', 123456789, 1);
INSERT INTO DEPARTEMENT (nom_departement, directeur, num_tel, code_h) VALUES ('Menage', 'Directeur Mengo', 0607080910, 1);
-- Pour le deuxieme hotel
INSERT INTO DEPARTEMENT (nom_departement, directeur, num_tel, code_h) VALUES ('Service Clientele', 'Directeur Patrick', 123456789, 2);
INSERT INTO DEPARTEMENT (nom_departement, directeur, num_tel, code_h) VALUES ('Cuisine', 'Directeur Cuisto', 0607080910, 2);
INSERT INTO DEPARTEMENT (nom_departement, directeur, num_tel, code_h) VALUES ('Reception', 'Directeur Reception', 123456789, 2);
INSERT INTO DEPARTEMENT (nom_departement, directeur, num_tel, code_h) VALUES ('Menage', 'Directeur Mengo', 0607080910, 2);





-- Ajout d'entrees dans la table RESERVATION_DATE
INSERT INTO RESERVATION_DATE (date_debut, date_fin) VALUES (TO_DATE('2023-01-01', 'YYYY-MM-DD'), TO_DATE('2023-01-10', 'YYYY-MM-DD'));
INSERT INTO RESERVATION_DATE (date_debut, date_fin) VALUES (TO_DATE('2023-02-01', 'YYYY-MM-DD'), TO_DATE('2023-02-10', 'YYYY-MM-DD'));
INSERT INTO RESERVATION_DATE (date_debut, date_fin) VALUES (TO_DATE('2023-03-01', 'YYYY-MM-DD'), TO_DATE('2023-03-10', 'YYYY-MM-DD'));
INSERT INTO RESERVATION_DATE (date_debut, date_fin) VALUES (TO_DATE('2023-04-01', 'YYYY-MM-DD'), TO_DATE('2023-04-10', 'YYYY-MM-DD'));

-- Ajout d'entrees dans la table COMMANDE
INSERT INTO COMMANDE (nom_commande, date_commande, prix, quantite, est_payee, nom_departement, num_chambre, code_h) VALUES ('Commande1', TO_DATE('2023-01-02', 'YYYY-MM-DD'), 100, 2, 'Y', 'Service Clientele', '1', 1);
INSERT INTO COMMANDE (nom_commande, date_commande, prix, quantite, est_payee, nom_departement, num_chambre, code_h) VALUES ('Commande2', TO_DATE('2023-02-02', 'YYYY-MM-DD'), 199.99, 3, 'Y', 'Service Clientele', '2', 1);
INSERT INTO COMMANDE (nom_commande, date_commande, prix, quantite, est_payee, nom_departement, num_chambre, code_h) VALUES ('Commande3', TO_DATE('2023-03-02', 'YYYY-MM-DD'), 365, 4, 'Y', 'Service Clientele', '3', 1);

-- Ajout d'entrees dans la table RESERVER
INSERT INTO RESERVER (date_debut, date_fin, num_client, code_h, num_chambre) VALUES (TO_DATE('2023-01-01', 'YYYY-MM-DD'), TO_DATE('2023-01-10', 'YYYY-MM-DD'), 1, 1, '1');
INSERT INTO RESERVER (date_debut, date_fin, num_client, code_h, num_chambre) VALUES (TO_DATE('2023-02-01', 'YYYY-MM-DD'), TO_DATE('2023-02-10', 'YYYY-MM-DD'), 2, 1, '2');
INSERT INTO RESERVER (date_debut, date_fin, num_client, code_h, num_chambre) VALUES (TO_DATE('2023-03-01', 'YYYY-MM-DD'), TO_DATE('2023-03-10', 'YYYY-MM-DD'), 3, 2, '80');

-- Ajout d'entrees dans la table RESEAU_HOTEL
insert into RESEAU_HOTEL (code_h, nom_departement, num_chambre, num_ident, date_debut, date_fin, date_commande, nom_commande) values (1, 'Service Clientele', '1', 1, TO_DATE('2023-01-01', 'YYYY-MM-DD'), TO_DATE('2023-01-10', 'YYYY-MM-DD'), TO_DATE('2023-01-02', 'YYYY-MM-DD'), 'Commande1');
insert into RESEAU_HOTEL (code_h, nom_departement, num_chambre, num_ident, date_debut, date_fin, date_commande, nom_commande) values (1, 'Service Clientele', '2', 2, TO_DATE('2023-02-01', 'YYYY-MM-DD'), TO_DATE('2023-02-10', 'YYYY-MM-DD'), TO_DATE('2023-02-02', 'YYYY-MM-DD'), 'Commande2');
insert into RESEAU_HOTEL (code_h, nom_departement, num_chambre, num_ident, date_debut, date_fin, date_commande, nom_commande) values (2, 'Service Clientele', '80', 3, TO_DATE('2023-03-01', 'YYYY-MM-DD'), TO_DATE('2023-03-10', 'YYYY-MM-DD'), TO_DATE('2023-03-02', 'YYYY-MM-DD'), 'Commande3');