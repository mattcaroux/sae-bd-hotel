-- Selection de tous les clients de la table
SELECT *
FROM PERSONNEL
NATURAL JOIN CLIENT;

-- Selectionner tous les hotels de la table
select * from HOTEL;

-- Toutes les reservations
select * from RESERVATION_DATE;

-- Toutes les chambres
select * from CHAMBRE;

-- Tous les clients
select * from CLIENT;

-- Tous les personnels
select * from PERSONNEL;

-- Toutes les personnes
select * from PERSONNE;

-- Tous les departements
select * from DEPARTEMENT;

-- Toutes les reservations
select * from RESERVER;

-- Toutes les commandes
select * from COMMANDE;

-- Tous le le reseau
select * from RESEAU_HOTEL;