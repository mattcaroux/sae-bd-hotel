-- supressions des tables
drop table RESERVER;
drop table COMMANDE;
drop table DEPARTEMENT;
drop table CHAMBRE;
drop table PERSONNEL;
drop table CLIENT;
drop table PERSONNE;
drop table HOTEL;
drop table RESERVATION_DATE;
-- drop table DATE;
purge recyclebin;
-- Creation de la table HOTEL
create table HOTEL (
  code_h number(11) NOT NULL,
  nom_h varchar(20),
  address varchar(20),
  nb_chambre number(11),
  qualite number(1),
  constraint CleHotel primary key (code_h)
);

-- Creation de la table PERSONNE
create table PERSONNE (
    num_ident number(11),
    nom varchar(20),
    adresse varchar(100),
    date_naissance date,
    constraint ClePersonne primary key (num_ident)
);

-- Creation de la table CLIENT
create table CLIENT (
    nationalite varchar(20),
    num_ident number(11), -- Ajout de la colonne num_ident pour la cle etrangère
    constraint fk_num_ident foreign key (num_ident) references PERSONNE (num_ident),
    constraint numClient primary key (num_ident, nationalite)
);
-- Creation de la table PERSONNEL
create table PERSONNEL(
    fonction varchar(20),
    num_ident number(11), -- Ajout de la colonne num_ident pour la cle etrangère
    constraint fk_personnel FOREIGN KEY (num_ident) REFERENCES PERSONNE (num_ident),
    constraint numPersonnel primary key (num_ident, fonction) -- Correction du nom de la contrainte
);

-- Creation de la table CHAMBRE
create table CHAMBRE (
    num_chambre varchar(20),
    capacite_max number(11),
    superficie number(11),
    code_h number(11), -- Ajout de la colonne code_h pour la cle etrangère
    constraint FaibleChambre FOREIGN KEY (code_h) REFERENCES HOTEL(code_h),
    constraint CleChambre primary key (num_chambre, code_h)
);

-- Creation de la table DEPARTEMENT
create table DEPARTEMENT (
    nom_departement varchar(20),
    directeur varchar(20),
    num_tel number(11),
    code_h number(11), -- Ajout de la colonne code_h pour la cle etrangère
    num_ident number(11),
    constraint FaibleDept FOREIGN KEY (code_h) REFERENCES HOTEL(code_h),
    constraint CleDepartement primary key (nom_departement, code_h)
);

create table RESERVATION_DATE (
    date_debut date,
    date_fin date,
    constraint CDate primary key (date_debut, date_fin)
);



-- Creation de la table COMMANDE
CREATE TABLE COMMANDE (
    nom_commande varchar(20),
    date_commande date,
    prix number(11),
    quantite number(11),
    est_payee char(1), -- Utilisation de CHAR(1) pour representer des valeurs de verite ou de faussete
    nom_departement varchar(50),
    num_chambre varchar(20),
    code_h number(11), 
    constraint NomDep FOREIGN KEY (nom_departement, code_h) REFERENCES DEPARTEMENT(nom_departement, code_h),
    constraint NumChambre FOREIGN KEY (num_chambre, code_h) REFERENCES CHAMBRE(num_chambre, code_h),
    constraint CleCommande primary key (nom_commande, num_chambre, nom_departement, date_commande)
);


-- Creation de la table RESERVER
create table RESERVER(
    date_debut date,
    date_fin date,
    num_client number(11),
    code_h number(11),
    num_chambre varchar(20),
    constraint ReserExist FOREIGN KEY (num_chambre, code_h) REFERENCES CHAMBRE(num_chambre, code_h),
    constraint DateReserv FOREIGN KEY (date_debut, date_fin) REFERENCES RESERVATION_DATE(date_debut, date_fin),
    constraint CleReserve primary key (num_chambre, code_h, num_client, date_fin)
);

create table RESEAU_HOTEL(
    code_h number(11),
    nom_departement varchar(20),
    num_chambre varchar(20),
    num_ident number(11),
    date_debut date,
    date_fin date,
    date_commande date,
    nom_commande varchar(20),
    constraint CleUnivers primary key (code_h, nom_departement, num_chambre, num_ident, date_debut, date_fin, date_commande, nom_commande)
);